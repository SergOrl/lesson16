package by.itacademy.lesson16.domain;

public interface Resource {
    String name();

    int value();
}
