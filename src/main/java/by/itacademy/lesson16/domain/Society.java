package by.itacademy.lesson16.domain;

import by.itacademy.lesson16.domain.queue.ResourcesQueue;
import by.itacademy.lesson16.domain.resources.Budget;
import by.itacademy.lesson16.domain.resources.Lands;
import by.itacademy.lesson16.domain.resources.Realty;
import by.itacademy.lesson16.domain.resources.Taxes;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Society {
    private List<Resource> random = new ArrayList<>();
    {
        random.add(new Lands());
        random.add(new Taxes());
        random.add(new Realty());
        random.add(new Budget());
    }
    private ResourcesQueue queue;

    public Society(ResourcesQueue queue) {
        this.queue = queue;
    }

    public void start() {
        final Random generator = new Random();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    sleep();
                    queue.add(random.get(generator.nextInt(4)));
                }
            }

            private void sleep() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}