package by.itacademy.lesson16.domain.queue;

import by.itacademy.lesson16.domain.Resource;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockResourceQueue implements ResourcesQueue {
    private final Queue<Resource> queue = new LinkedList<>();
    private Lock lock = new ReentrantLock();
    private Condition emptyQueue = lock.newCondition();

    @Override
    public void add(Resource resource) {
        lock.lock();
        try {
            queue.add(resource);
            emptyQueue.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Resource retrieve() {
        lock.lock();
        try {
            while (queue.isEmpty()){
                try {
                    emptyQueue.await();
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e.getMessage());
                }
            }
            return queue.poll();
        } finally {
            lock.unlock();
        }
    }
}