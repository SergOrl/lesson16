package by.itacademy.lesson16.domain.queue;

import by.itacademy.lesson16.domain.Resource;
import by.itacademy.lesson16.domain.queue.ResourcesQueue;

import java.util.LinkedList;
import java.util.Queue;

public class SynchronizedResourcesQueue implements ResourcesQueue {
    private final Queue<Resource> queue = new LinkedList<>();

    public void add(Resource resource) {
        synchronized (queue) {
            queue.add(resource);
            queue.notify();
        }
    }

    public Resource retrieve() {
        synchronized (queue) {
            while (queue.isEmpty())
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            return queue.poll();
        }
    }
}