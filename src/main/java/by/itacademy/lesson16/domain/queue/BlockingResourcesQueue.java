package by.itacademy.lesson16.domain.queue;

import by.itacademy.lesson16.domain.Resource;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockingResourcesQueue implements ResourcesQueue {
    private volatile static BlockingResourcesQueue instance;
    private final BlockingQueue<Resource> blockingQueue = new LinkedBlockingQueue<>();

    private BlockingResourcesQueue() {
    }

    public static BlockingResourcesQueue instance() {
        if (instance == null) {
            synchronized (BlockingResourcesQueue.class) {
                if (instance == null)
                    instance = new BlockingResourcesQueue();

            }
        }
        return instance;
    }

    @Override
    public void add(Resource resource) {
        try {
            blockingQueue.put(resource);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    @Override
    public Resource retrieve() {
        try {
            return blockingQueue.take();
        } catch (InterruptedException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }
}