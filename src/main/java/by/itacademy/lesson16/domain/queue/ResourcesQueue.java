package by.itacademy.lesson16.domain.queue;

import by.itacademy.lesson16.domain.Resource;

public interface ResourcesQueue {
    public void add(Resource resource);

    Resource retrieve();
}
