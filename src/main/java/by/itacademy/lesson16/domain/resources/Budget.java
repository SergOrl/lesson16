package by.itacademy.lesson16.domain.resources;

import by.itacademy.lesson16.domain.Resource;

public class Budget implements Resource {
    @Override
    public String name() {
        return "Budget";
    }

    @Override
    public int value() {
        return 1_000_000_000;
    }

    @Override
    public String toString() {
        return "Budget{" +
                "name='" + name() + '\'' +
                ", value=" + value() +
                '}';
    }
}