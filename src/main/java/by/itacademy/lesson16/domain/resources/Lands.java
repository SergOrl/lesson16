package by.itacademy.lesson16.domain.resources;

import by.itacademy.lesson16.domain.Resource;

public class Lands implements Resource {
    @Override
    public String name() {
        return "Lands";
    }

    @Override
    public int value() {
        return 1_000_000_000;
    }

    @Override
    public String toString() {
        return "Lands{" +
                "name='" + name() + '\'' +
                ", value=" + value() +
                '}';
    }
}