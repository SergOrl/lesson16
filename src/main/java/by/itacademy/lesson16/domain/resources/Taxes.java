package by.itacademy.lesson16.domain.resources;

import by.itacademy.lesson16.domain.Resource;

public class Taxes implements Resource {
    @Override
    public String name() {
        return "Taxes";
    }

    @Override
    public int value() {
        return 500_000;
    }

    @Override
    public String toString() {
        return "Taxes{" +
                "name='" + name() + '\'' +
                ", value=" + value() +
                '}';
    }
}