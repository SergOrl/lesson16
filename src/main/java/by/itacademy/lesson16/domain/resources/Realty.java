package by.itacademy.lesson16.domain.resources;

import by.itacademy.lesson16.domain.Resource;

public class Realty implements Resource {
    @Override
    public String name() {
        return "Realty";
    }

    @Override
    public int value() {
        return 2_000_000;
    }

    @Override
    public String toString() {
        return "Realty{" +
                "name='" + name() + '\'' +
                ", value=" + value() +
                '}';
    }
}