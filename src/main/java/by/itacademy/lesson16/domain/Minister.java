package by.itacademy.lesson16.domain;

import by.itacademy.lesson16.domain.queue.ResourcesQueue;

import java.util.ArrayList;
import java.util.List;

public class Minister {
    private String responsibility;
    private List<Resource> properties = new ArrayList<>();
    private ResourcesQueue queue;

    public Minister(String responsibility, ResourcesQueue queue) {
        this.responsibility = responsibility;
        this.queue = queue;
    }

    public void add(Resource resource) {
        properties.add(resource);
    }

    public void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    sleep();
                    Resource resource = queue.retrieve();
                    add(resource);
                    System.out.println(Minister.this);
                }
            }

            private void sleep() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public String toString() {
        return "Minister{" +
                "responsibility='" + responsibility + '\'' +
                ", properties=" + properties +
                '}';
    }
}