package by.itacademy.lesson16;

import by.itacademy.lesson16.domain.Minister;
import by.itacademy.lesson16.domain.Society;
import by.itacademy.lesson16.domain.queue.SynchronizedResourcesQueue;

public class App {
    public static void main(String[] args) {
        SynchronizedResourcesQueue queue = new SynchronizedResourcesQueue();
        new Minister("Health care", queue).start();
        new Minister("Agriculture", queue).start();
        new Minister("Finance", queue).start();
        new Society(queue).start();
    }
}